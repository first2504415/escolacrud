﻿using EscolaCRUD.Models;
using EscolaCRUD.Repositories;
using EscolaCRUD.Services;
using Xunit;
using Moq;

namespace EscolaCRUDTest
{
    public class AlunoServiceTests
    {
        [Fact]
        public void GetAllAlunos_ReturnsAllAlunos()
        {
            // Arrange
            var mockRepository = new Mock<IAlunoRepository>();
            mockRepository.Setup(repo => repo.GetAll()).Returns(new List<Aluno>
            {
                new Aluno { CodAluno = 1, Nome = "Aluno 1", Nascimento = new DateTime(2000, 1, 1), CPF = "12345678900", Endereco = "Rua 1", Celular = "123456789" },
                new Aluno { CodAluno = 2, Nome = "Aluno 2", Nascimento = new DateTime(2000, 1, 1), CPF = "12345678901", Endereco = "Rua 2", Celular = "123456780" }
            });

            var service = new AlunoService(mockRepository.Object);

            // Act
            var result = service.GetAllAlunos();

            // Assert
            Assert.Collection(result,
                aluno =>
                {
                    Assert.Equal(1, aluno.CodAluno);
                    Assert.Equal("Aluno 1", aluno.Nome);
                },
                aluno =>
                {
                    Assert.Equal(2, aluno.CodAluno);
                    Assert.Equal("Aluno 2", aluno.Nome);
                });
        }

        [Fact]
        public void GetAlunoByCod_WithValidCod_ReturnsAluno()
        {
            // Arrange
            var cod = 1;
            var expectedAluno = new Aluno { CodAluno = cod, Nome = "Aluno 1" };
            var mockRepository = new Mock<IAlunoRepository>();
            mockRepository.Setup(repo => repo.GetByCod(cod)).Returns(expectedAluno);
            var service = new AlunoService(mockRepository.Object);

            // Act
            var result = service.GetAlunoByCod(cod);

            // Assert
            Assert.Equal(expectedAluno, result);
        }

        [Fact]
        public void GetAlunoByCod_WithInvalidCod_ThrowsKeyNotFoundException()
        {
            // Arrange
            var cod = 1;
            var mockRepository = new Mock<IAlunoRepository>();
            mockRepository.Setup(repo => repo.GetByCod(cod)).Throws(new KeyNotFoundException());
            var service = new AlunoService(mockRepository.Object);

            // Act & Assert
            Assert.Throws<KeyNotFoundException>(() => service.GetAlunoByCod(cod));
        }

        [Fact]
        public void AddAluno_ValidAluno_AddsAlunoToRepository()
        {
            // Arrange
            var alunoToAdd = new Aluno { CodAluno = 1, Nome = "Novo Aluno" };
            var mockRepository = new Mock<IAlunoRepository>();
            var service = new AlunoService(mockRepository.Object);

            // Act
            service.AddAluno(alunoToAdd);

            // Assert
            mockRepository.Verify(repo => repo.Add(alunoToAdd), Times.Once);
        }

        [Fact]
        public void UpdateAluno_ValidAluno_UpdatesAlunoInRepository()
        {
            // Arrange
            var cod = 1;
            var updatedAluno = new Aluno { CodAluno = cod, Nome = "Aluno Atualizado" };
            var mockRepository = new Mock<IAlunoRepository>();
            var service = new AlunoService(mockRepository.Object);

            // Act
            service.UpdateAluno(cod, updatedAluno);

            // Assert
            mockRepository.Verify(repo => repo.Update(cod, updatedAluno), Times.Once);
        }

        [Fact]
        public void DeleteAluno_WithValidCod_DeletesAlunoFromRepository()
        {
            // Arrange
            var cod = 1;
            var mockRepository = new Mock<IAlunoRepository>();
            var service = new AlunoService(mockRepository.Object);

            // Act
            service.DeleteAluno(cod);

            // Assert
            mockRepository.Verify(repo => repo.Delete(cod), Times.Once);
        }
    }

}

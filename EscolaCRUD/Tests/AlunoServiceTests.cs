﻿using EscolaCRUD.Models;
using EscolaCRUD.Repositories;
using EscolaCRUD.Services;
using Xunit;
using Moq;

namespace EscolaCRUD.Tests
{
    public class AlunoServiceTests
    {
        [Fact]
        public void GetAllAlunos_ReturnsAllAlunos()
        {
            // Arrange
            var mockRepository = new Mock<IAlunoRepository>();
            mockRepository.Setup(repo => repo.GetAll()).Returns(new List<Aluno>
            {
                new Aluno { CodAluno = 1, Nome = "Aluno 1", Nascimento = new DateTime(2000, 1, 1), CPF = "12345678900", Endereco = "Rua 1", Celular = "123456789" },
                new Aluno { CodAluno = 2, Nome = "Aluno 2", Nascimento = new DateTime(2000, 1, 1), CPF = "12345678901", Endereco = "Rua 2", Celular = "123456780" }
            });
            var service = new AlunoService(mockRepository.Object);

            // Act
            var result = service.GetAllAlunos();

            // Assert
            Assert.Collection(result,
                aluno =>
                {
                    Assert.Equal(1, aluno.CodAluno);
                    Assert.Equal("Aluno 1", aluno.Nome);
                },
                aluno =>
                {
                    Assert.Equal(2, aluno.CodAluno);
                    Assert.Equal("Aluno 2", aluno.Nome);
                });
        }
    }

}

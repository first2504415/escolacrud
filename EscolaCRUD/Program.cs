using EscolaCRUD.Repositories;
using EscolaCRUD.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add repositories
builder.Services.AddSingleton<IAlunoRepository, AlunoRepository>();
builder.Services.AddSingleton<IMateriaRepository, MateriaRepository>();
builder.Services.AddSingleton<INotaRepository, NotaRepository>();

// Add services
builder.Services.AddScoped<IAlunoService, AlunoService>();
builder.Services.AddScoped<IMateriaService, MateriaService>();
builder.Services.AddScoped<INotaService, NotaService>();

// Add controllers
builder.Services.AddControllers();

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Escola CRUD",
        Description = "Aplica��o para controle de notas em uma escola"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseAuthentication();

app.MapControllers();

app.Run();

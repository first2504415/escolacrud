﻿using EscolaCRUD.Models;
using EscolaCRUD.Repositories;

namespace EscolaCRUD.Services
{
    public class NotaService : INotaService
    {
        private readonly INotaRepository _notaRepository;

        public NotaService(INotaRepository notaRepository)
        {
            _notaRepository = notaRepository ?? throw new ArgumentNullException(nameof(notaRepository));

            PopulateData();
        }

        public IEnumerable<Nota> GetAllNotas()
        {
            return _notaRepository.GetAll();
        }

        public Nota GetNotaByCod(int cod)
        {
            try
            {
                return _notaRepository.GetByCod(cod);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        public List<Nota> GetNotaByCodAluno(int codAluno)
        {
            try
            {
                return _notaRepository.GetByCodAluno(codAluno);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        public void AddNota(Nota nota)
        {
            if (nota == null)
            {
                throw new ArgumentNullException(nameof(nota));
            }

            _notaRepository.Add(nota);
        }

        public void UpdateNota(int cod, Nota nota)
        {
            if (nota == null)
            {
                throw new ArgumentNullException(nameof(nota));
            }

            try
            {
                _notaRepository.Update(cod, nota);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        public void DeleteNota(int cod)
        {
            try
            {
                _notaRepository.Delete(cod);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        private void PopulateData()
        {
            Random rand = new Random();

            for (int i = 1; i <= 10; i++)
            {
                var codAluno = rand.Next(1, 6);
                var codMateria = rand.Next(1, 6);
                var notaFinal = rand.Next(1, 11);
                _notaRepository.Add(new Nota { CodNota = i, CodAluno = codAluno, CodMateria = codMateria, NotaFinal = notaFinal});
            }
        }
    }
}

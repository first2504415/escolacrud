﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public interface IAlunoService
    {
        IEnumerable<Aluno> GetAllAlunos();
        Aluno GetAlunoByCod(int cod);
        void AddAluno(Aluno aluno);
        void UpdateAluno(int cod, Aluno aluno);
        void DeleteAluno(int cod);
    }
}

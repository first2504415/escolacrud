﻿using EscolaCRUD.Models;
using EscolaCRUD.Repositories;

namespace EscolaCRUD.Services
{
    public class AlunoService : IAlunoService
    {
        private readonly IAlunoRepository _alunoRepository;

        public AlunoService(IAlunoRepository alunoRepository)
        {
            _alunoRepository = alunoRepository ?? throw new ArgumentNullException(nameof(alunoRepository));

            PopulateData();
        }

        public IEnumerable<Aluno> GetAllAlunos()
        {
            return _alunoRepository.GetAll();
        }

        public Aluno GetAlunoByCod(int cod)
        {
            try
            {
                return _alunoRepository.GetByCod(cod);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        public void AddAluno(Aluno aluno)
        {
            if (aluno == null)
            {
                throw new ArgumentNullException(nameof(aluno));
            }

            _alunoRepository.Add(aluno);
        }

        public void UpdateAluno(int cod, Aluno aluno)
        {
            if (aluno == null)
            {
                throw new ArgumentNullException(nameof(aluno));
            }

            try
            {
                _alunoRepository.Update(cod, aluno);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        public void DeleteAluno(int cod)
        {
            try
            {
                _alunoRepository.Delete(cod);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        private void PopulateData()
        {
            for (int i = 1; i <= 5; i++)
            {
                _alunoRepository.Add(new Aluno { CodAluno = i, Nome = $"Aluno {i}", Nascimento = new DateTime(2000, 1, 1), CPF = "12345678900", Endereco = $"Rua {i}", Celular = "123456789" });
            }
        }
    }
}

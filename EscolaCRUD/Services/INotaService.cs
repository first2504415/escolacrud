﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public interface INotaService
    {
        IEnumerable<Nota> GetAllNotas();
        List<Nota> GetNotaByCodAluno(int codAluno);
        Nota GetNotaByCod(int cod);
        void AddNota(Nota nota);
        void UpdateNota(int cod, Nota nota);
        void DeleteNota(int cod);
    }
}

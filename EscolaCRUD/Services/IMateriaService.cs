﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public interface IMateriaService
    {
        IEnumerable<Materia> GetAllMaterias();
        Materia GetMateriaByCod(int cod);
        void AddMateria(Materia materia);
        void UpdateMateria(int cod, Materia materia);
        void DeleteMateria(int cod);
    }
}

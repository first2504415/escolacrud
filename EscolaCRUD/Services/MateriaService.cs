﻿using EscolaCRUD.Models;
using EscolaCRUD.Repositories;

namespace EscolaCRUD.Services
{
    public class MateriaService : IMateriaService
    {
        private readonly IMateriaRepository _materiaRepository;

        public MateriaService(IMateriaRepository materiaRepository)
        {
            _materiaRepository = materiaRepository ?? throw new ArgumentNullException(nameof(materiaRepository));

            PopulateData();
        }

        public IEnumerable<Materia> GetAllMaterias()
        {
            return _materiaRepository.GetAll();
        }

        public Materia GetMateriaByCod(int cod)
        {
            try
            {
                return _materiaRepository.GetByCod(cod);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        public void AddMateria(Materia materia)
        {
            if (materia == null)
            {
                throw new ArgumentNullException(nameof(materia));
            }

            _materiaRepository.Add(materia);
        }

        public void UpdateMateria(int cod, Materia materia)
        {
            if (materia == null)
            {
                throw new ArgumentNullException(nameof(materia));
            }

            try
            {
                _materiaRepository.Update(cod, materia);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        public void DeleteMateria(int cod)
        {
            try
            {
                _materiaRepository.Delete(cod);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
        }

        private void PopulateData()
        {
            for (int i = 1; i <= 5; i++)
            {
                _materiaRepository.Add(new Materia { CodMateria = i, Descricao = $"Materia Escolar #{i}"});
            }
        }
    }
}

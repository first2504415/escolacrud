﻿namespace EscolaCRUD.Models
{
    public class Auth
    {
        public int? CodAluno { get; set; }
        public string? DefaultToken { get; set; }
        public string? SessionToken { get; set; }
    }
}

﻿namespace EscolaCRUD.Models
{
    public class Nota
    {
        public int CodNota { get; set; }
        public int CodAluno { get; set; }
        public int CodMateria { get; set; }
        public double NotaFinal { get; set; }

    }
}

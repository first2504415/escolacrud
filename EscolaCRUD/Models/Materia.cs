﻿namespace EscolaCRUD.Models
{
    public class Materia
    {
        public int CodMateria { get; set; }
        public string Descricao { get; set; }
    }
}

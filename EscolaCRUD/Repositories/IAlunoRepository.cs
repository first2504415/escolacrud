﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public interface IAlunoRepository
    {
        IEnumerable<Aluno> GetAll();
        Aluno GetByCod(int cod);
        void Add(Aluno aluno);
        void Update(int cod, Aluno aluno);
        void Delete(int cod);
    }
}

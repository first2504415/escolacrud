﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public class AlunoRepository : IAlunoRepository
    {
        private readonly List<Aluno> _alunos = new List<Aluno>();

        public IEnumerable<Aluno> GetAll() => _alunos;

        public Aluno GetByCod(int cod)
        {
            var existingAluno = _alunos.FirstOrDefault(a => a.CodAluno == cod);

            if (existingAluno == null)
            {
                throw new KeyNotFoundException($"Aluno com código {cod} não foi encontrado");
            }

            return existingAluno;
        }

        public void Add(Aluno aluno)
        {
            if (aluno == null)
            {
                throw new ArgumentNullException(nameof(aluno));
            }

            _alunos.Add(aluno);
        }

        public void Update(int cod, Aluno aluno)
        {
            var existingAluno = _alunos.FirstOrDefault(a => a.CodAluno == cod);
            
            if (existingAluno == null)
            {
                throw new KeyNotFoundException($"Aluno com código {cod} não foi encontrado");
            }

            existingAluno.Nome = aluno.Nome;
            existingAluno.Nascimento = aluno.Nascimento;
            existingAluno.CPF = aluno.CPF;
            existingAluno.Endereco = aluno.Endereco;
            existingAluno.Celular = aluno.Celular;
        }

        public void Delete(int cod)
        {
            var existingAluno = _alunos.FirstOrDefault(a => a.CodAluno == cod);
            
            if (existingAluno == null)
            {
                throw new KeyNotFoundException($"Aluno com código {cod} não foi encontrado");
            }
            
            _alunos.Remove(existingAluno);
        }
    }
}

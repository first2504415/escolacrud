﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public interface INotaRepository
    {
        IEnumerable<Nota> GetAll();
        List<Nota> GetByCodAluno(int codAluno);
        Nota GetByCod(int cod);
        void Add(Nota nota);
        void Update(int cod, Nota nota);
        void Delete(int cod);
    }
}

﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public interface IMateriaRepository
    {
        IEnumerable<Materia> GetAll();
        Materia GetByCod(int cod);
        void Add(Materia materia);
        void Update(int cod, Materia materia);
        void Delete(int cod);
    }
}

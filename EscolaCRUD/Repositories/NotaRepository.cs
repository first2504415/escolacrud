﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public class NotaRepository : INotaRepository
    {
        private readonly List<Nota> _notas = new List<Nota>();

        public IEnumerable<Nota> GetAll() => _notas;

        public Nota GetByCod(int cod)
        {
            var existingNota = _notas.FirstOrDefault(a => a.CodNota == cod);

            if (existingNota == null)
            {
                throw new KeyNotFoundException($"Nota com código {cod} não foi encontrada");
            }

            return existingNota;
        }

        public List<Nota> GetByCodAluno(int codAluno)
        {
            if (codAluno <= 0)
            {
                throw new ArgumentNullException(nameof(codAluno));
            }

            List<Nota> notas = _notas.Where(n => n.CodAluno == codAluno).ToList();

            return notas;
        }

        public void Add(Nota nota)
        {
            if (nota == null)
            {
                throw new ArgumentNullException(nameof(nota));
            }

            _notas.Add(nota);
        }

        public void Update(int cod, Nota nota)
        {
            var existingNota = _notas.FirstOrDefault(a => a.CodNota == cod);
            
            if (existingNota == null)
            {
                throw new KeyNotFoundException($"Nota com código {cod} não foi encontrada");
            }

            existingNota.CodAluno = nota.CodAluno;
            existingNota.CodMateria = nota.CodMateria;
            existingNota.NotaFinal = nota.NotaFinal;
        }

        public void Delete(int cod)
        {
            var existingNota = _notas.FirstOrDefault(a => a.CodNota == cod);
            
            if (existingNota == null)
            {
                throw new KeyNotFoundException($"Nota com código {cod} não foi encontrada");
            }
            
            _notas.Remove(existingNota);
        }
    }
}

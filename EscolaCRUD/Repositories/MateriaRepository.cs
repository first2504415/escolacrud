﻿using EscolaCRUD.Models;

namespace EscolaCRUD.Repositories
{
    public class MateriaRepository : IMateriaRepository
    {
        private readonly List<Materia> _materias = new List<Materia>();

        public IEnumerable<Materia> GetAll() => _materias;

        public Materia GetByCod(int cod)
        {
            var existingMateria = _materias.FirstOrDefault(a => a.CodMateria == cod);

            if (existingMateria == null)
            {
                throw new KeyNotFoundException($"Materia com código {cod} não foi encontrada");
            }

            return existingMateria;
        }

        public void Add(Materia materia)
        {
            if (materia == null)
            {
                throw new ArgumentNullException(nameof(materia));
            }

            _materias.Add(materia);
        }

        public void Update(int cod, Materia materia)
        {
            var existingMateria = _materias.FirstOrDefault(a => a.CodMateria == cod);
            
            if (existingMateria == null)
            {
                throw new KeyNotFoundException($"Materia com código {cod} não foi encontrada");
            }

            existingMateria.Descricao = materia.Descricao;
        }

        public void Delete(int cod)
        {
            var existingMateria = _materias.FirstOrDefault(a => a.CodMateria == cod);
            
            if (existingMateria == null)
            {
                throw new KeyNotFoundException($"Materia com código {cod} não foi encontrada");
            }

            _materias.Remove(existingMateria);
        }
    }
}

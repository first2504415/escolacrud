using EscolaCRUD.Models;
using EscolaCRUD.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace EscolaCRUD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MateriaController : ControllerBase
    {
        private readonly IMateriaService _materiaService;

        public MateriaController(IMateriaService materiaService)
        {
            _materiaService = materiaService ?? throw new ArgumentNullException(nameof(materiaService));
        }

        /// <summary>
        /// Consulta todas as mat�ria da base
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllMaterias()
        {
            try
            {
                var materias = _materiaService.GetAllMaterias();

                return Ok(materias);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao recuperar materias: {ex.Message}");
            }
        }

        /// <summary>
        /// Consulta uma mat�ria com base no c�digo
        /// </summary>
        /// <param name="cod">C�digo da mat�ria</param>
        /// <returns></returns>
        [HttpGet("{cod}")]
        public IActionResult GetMateriaByCod(int cod)
        {
            try
            {
                var materia = _materiaService.GetMateriaByCod(cod);

                return Ok(materia);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao recuperar materia com c�digo {cod}: {ex.Message}");
            }
        }

        /// <summary>
        /// Adiciona uma mat�ria na base
        /// </summary>
        /// <param name="materia">Objeto da materia a ser adicionada</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddMateria([FromBody] Materia materia)
        {
            try
            {
                _materiaService.AddMateria(materia);

                return CreatedAtAction(nameof(GetMateriaByCod), new { id = materia.CodMateria }, materia);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao adicionar materia: {ex.Message}");
            }
        }

        /// <summary>
        /// Atualiza mat�ria na base
        /// </summary>
        /// <param name="cod">C�digo da mat�ria</param>
        /// <param name="materia">Objeto da mat�ria</param>
        /// <returns></returns>
        [HttpPut("{cod}")]
        public IActionResult UpdateMateria(int cod, [FromBody] Materia materia)
        {
            try
            {
                _materiaService.UpdateMateria(cod, materia);

                return NoContent();
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao atualizar materia com c�digo {cod}: {ex.Message}");
            }
        }

        /// <summary>
        /// Remove registro de mat�ria da base
        /// </summary>
        /// <param name="cod">C�digo da mat�ria</param>
        /// <returns></returns>
        [HttpDelete("{cod}")]
        public IActionResult DeleteMateria(int cod)
        {
            try
            {
                _materiaService.DeleteMateria(cod);

                return NoContent();
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao deletar materia com c�digo {cod}: {ex.Message}");
            }
        }
    }

}
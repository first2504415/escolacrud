using EscolaCRUD.Models;
using EscolaCRUD.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace EscolaCRUD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotaController : ControllerBase
    {
        private readonly INotaService _notaService;

        public NotaController(INotaService notaService)
        {
            _notaService = notaService ?? throw new ArgumentNullException(nameof(notaService));
        }

        /// <summary>
        /// Consulta todas as notas da base
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllNotas()
        {
            try
            {
                var notas = _notaService.GetAllNotas();

                return Ok(notas);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao recuperar notas: {ex.Message}");
            }
        }

        /// <summary>
        /// Consulta uma nota com base no c�digo
        /// </summary>
        /// <param name="cod">C�digo da nota</param>
        /// <returns></returns>
        [HttpGet("{cod}")]
        public IActionResult GetNotaByCod(int cod)
        {
            try
            {
                var nota = _notaService.GetNotaByCod(cod);

                return Ok(nota);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao recuperar nota com c�digo {cod}: {ex.Message}");
            }
        }

        /// <summary>
        /// Consulta uma nota com base no c�digo do aluno
        /// </summary>
        /// <param name="codAluno">C�digo do aluno</param>
        /// <returns></returns>
        [HttpGet("consultaNota/{codAluno}")]
        public IActionResult GetNotaByCodAluno(int codAluno)
        {
            try
            {
                var notas = _notaService.GetNotaByCodAluno(codAluno);

                return Ok(notas);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao recuperar notas com c�digo de aluno {codAluno}: {ex.Message}");
            }
        }

        /// <summary>
        /// Adiciona uma nota na base
        /// </summary>
        /// <param name="nota">Objeto da nota a ser adicionada</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddNota([FromBody] Nota nota)
        {
            try
            {
                _notaService.AddNota(nota);

                return CreatedAtAction(nameof(GetNotaByCod), new { id = nota.CodNota }, nota);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao adicionar nota: {ex.Message}");
            }
        }

        /// <summary>
        /// Atualiza nota na base
        /// </summary>
        /// <param name="cod">C�digo da nota</param>
        /// <param name="nota">Objeto da nota</param>
        /// <returns></returns>
        [HttpPut("{cod}")]
        public IActionResult UpdateNota(int cod, [FromBody] Nota nota)
        {
            try
            {
                _notaService.UpdateNota(cod, nota);

                return NoContent();
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao atualizar nota com c�digo {cod}: {ex.Message}");
            }
        }

        /// <summary>
        /// Remove registro de nota da base
        /// </summary>
        /// <param name="cod">C�digo da nota</param>
        /// <returns></returns>
        [HttpDelete("{cod}")]
        public IActionResult DeleteNota(int cod)
        {
            try
            {
                _notaService.DeleteNota(cod);

                return NoContent();
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao deletar nota com c�digo {cod}: {ex.Message}");
            }
        }
    }

}
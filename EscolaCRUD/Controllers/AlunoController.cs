using EscolaCRUD.Models;
using EscolaCRUD.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EscolaCRUD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AlunoController : ControllerBase
    {
        private readonly IAlunoService _alunoService;

        public AlunoController(IAlunoService alunoService)
        {
            _alunoService = alunoService ?? throw new ArgumentNullException(nameof(alunoService));
        }

        /// <summary>
        /// Consulta todos os alunos da base
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllAlunos()
        {
            try
            {
                var alunos = _alunoService.GetAllAlunos();

                return Ok(alunos);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao recuperar alunos: {ex.Message}");
            }
        }

        /// <summary>
        /// Consulta um aluno com base no c�digo
        /// </summary>
        /// <param name="cod">C�digo do aluno</param>
        /// <returns></returns>
        [HttpGet("{cod}")]
        public IActionResult GetAlunoByCod(int cod)
        {
            try
            {
                var aluno = _alunoService.GetAlunoByCod(cod);

                return Ok(aluno);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao recuperar aluno com c�digo {cod}: {ex.Message}");
            }
        }

        /// <summary>
        /// Adiciona um aluno na base
        /// </summary>
        /// <param name="aluno">Objeto do aluno a ser adicionado</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddAluno([FromBody] Aluno aluno)
        {
            try
            {
                _alunoService.AddAluno(aluno);

                return CreatedAtAction(nameof(GetAlunoByCod), new { id = aluno.CodAluno }, aluno);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao adicionar aluno: {ex.Message}");
            }
        }

        /// <summary>
        /// Atualiza aluno na base
        /// </summary>
        /// <param name="cod">C�digo do aluno</param>
        /// <param name="aluno">Objeto do aluno</param>
        /// <returns></returns>
        [HttpPut("{cod}")]
        public IActionResult UpdateAluno(int cod, [FromBody] Aluno aluno)
        {
            try
            {
                _alunoService.UpdateAluno(cod, aluno);

                return NoContent();
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao atualizar aluno com c�digo {cod}: {ex.Message}");
            }
        }

        /// <summary>
        /// Remove registro de aluno da base
        /// </summary>
        /// <param name="cod">C�digo do aluno</param>
        /// <returns></returns>
        [HttpDelete("{cod}")]
        public IActionResult DeleteAluno(int cod)
        {
            try
            {
                _alunoService.DeleteAluno(cod);

                return NoContent();
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erro ao deletar aluno com c�digo {cod}: {ex.Message}");
            }
        }
    }

}